#pragma once
#include "rodos.h"

struct attitude
{
    float yaw;
    float pitch;
    float roll;
};
