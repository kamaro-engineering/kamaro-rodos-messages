#pragma once
#include "rodos.h"

struct covariance3d {
  float c11;
  float c12;
  float c13;
  float c21;
  float c22;
  float c23;
  float c31;
  float c32;
  float c33;
};
