#pragma once
#include "rodos.h"

template<size_t n>
class VectorMessage {

    private:

    float v[n];

    public:

    float& operator[](int idx) { return v[idx]; }

    const float& operator[](int idx) const { return v[idx]; }

    float& at(size_t idx) {
        if (idx >= n) {
            PRINTF("vector out of range\n");
        }
        return (*this)[idx];
    }

    const float& at(size_t idx) const {
        if (idx >= n) {
            PRINTF("vector out of range\n");
        }
        return (*this)[idx];
    }

    constexpr size_t size() const noexcept {
        return n * sizeof(float);
    }

    constexpr size_t length() const noexcept {
        return n;
    }
};
