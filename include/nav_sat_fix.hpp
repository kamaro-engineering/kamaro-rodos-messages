#pragma once
#include "rodos.h"
#include "covariance.hpp"

// gga
struct nav_sat_fix {
  uint8_t fix_type;
  float latitude;
  float longitude;
  float altitude;
  covariance3d position_covariance;
};
