#pragma once
#include "rodos.h"

#include "point_msg.hpp"
#include "quaternion_msg.hpp"

struct pose {
  point position;
  quaternion orientation; 
};
