#pragma once
#include "rodos.h"

// vtg
struct nav_sat_track {
  float true_track_degrees;
  float true_track_magnetic;
  float velocity_kph;
};
