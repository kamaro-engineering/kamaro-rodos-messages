#pragma once
#include "rodos.h"

struct ping {
  uint32_t sender_id;
  uint32_t receiver_id;
  uint32_t hop_count;
  uint32_t data;
};
