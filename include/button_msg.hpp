#pragma once
#include "rodos.h"

const int32_t BUTTON_MSG_USER_BUTTON_ID = 1;

struct button_short_pressed
{
    int32_t button_id;
};

struct button_long_pressed
{
    int32_t button_id;
};

struct button_pressed
{
    int32_t button_id;
    int64_t duration;
};
