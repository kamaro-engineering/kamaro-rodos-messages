#pragma once
#include "rodos.h"

struct quaternion {
  float r;
  float i;
  float j;
  float k;

  quaternion() = default;

  constexpr quaternion(float r, float i, float j, float k) noexcept:
  r(r), i(i), j(j), k(k)
  {}
};
