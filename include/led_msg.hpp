#pragma once
#include "rodos.h"

struct led {
  bool is_enabled;
  int64_t period;
  float dutycycle;
};
